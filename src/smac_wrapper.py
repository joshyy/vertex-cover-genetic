#!/usr/bin/python

#Usage: ./vc-genetic <rand-seed> <pop-size> <reproduction-number> <conflict-penalty> <select-out-of> <mutation-probability> <mutation-ratio> <flip-probability> <nr-partitions> <complement-ratio>

from argparse import ArgumentParser;
import os;

parser = ArgumentParser()
parser.add_argument("instance", nargs="?");
parser.add_argument("algo", nargs="?");
parser.add_argument("instance_specifics", nargs="?");
parser.add_argument("runtime_cutoff", nargs="?");
parser.add_argument("runlength", nargs="?");
parser.add_argument("-N", "--pop-size", dest="pop_size");
parser.add_argument("-r", "--reproduction-number", dest="reproduction_number");
parser.add_argument("-c", "--conflict-penalty", dest="conflict_penalty");
parser.add_argument("-o", "--select-out-of", dest="select_out_of");
parser.add_argument("-p", "--mutation-probability", dest="mutation_probability");
parser.add_argument("-m", "--mutation-ratio", dest="mutation_ratio");
parser.add_argument("-f", "--flip-probability", dest="flip_probability");
parser.add_argument("-n", "--nr-partitions", dest="nr_partitions");
parser.add_argument("-a", "--complement-ratio", dest="complement_ratio");

args = parser.parse_args();

val = int(os.popen("timeout 1m ./vc-genetic " + str(84) + " " + args.pop_size + " " + args.reproduction_number +
        " " + args.conflict_penalty + " " + args.select_out_of + " " + args.mutation_probability +
        " " + args.mutation_ratio + " " + args.flip_probability +
        " " + args.nr_partitions + " " + args.complement_ratio + " < " + args.instance).read());
print("Result for SMAC: SUCCESS, 0, 0, " + str(val) + ", 0");
