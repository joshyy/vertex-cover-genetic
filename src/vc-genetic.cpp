#include <algorithm>
#include <cmath>
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <queue>
#include <random>
#include <string>

#include "graph.h"

struct par_t {
  std::mt19937 rng;
  size_t population_size;
  size_t reproduction_number;
  double conflict_penalty;
  int select_out_of;
  double mutation_probability; // probability for each solution to be mutated
  double mutation_ratio;       // ratio of bits that have a chance to be flipped
  double flip_probability; // probability for each of those bits to be flipped
  int nr_partitions;
  double complement_ratio;
};

typedef std::vector<bool> sol_t;

volatile bool signalled;

std::vector<bool> get_partition(const graph &g, par_t &par);
void init(int argc, char **argv, graph &g, std::vector<sol_t> &population,
          par_t &par, std::vector<std::vector<bool>> &partitions);
template <class T, class RNG>
void choose_n_randomly(std::vector<T> &vec, RNG &rng, size_t n);
unsigned int count_unreached_edges(const graph &g, const sol_t sol);
double fitness(const graph &g, par_t &par, const sol_t &sol);
void mutation(const graph &g, par_t &par, sol_t &sol);
sol_t cross_over(const graph &g, par_t &par,
                 const std::vector<std::vector<bool>> &partitions,
                 const sol_t &sol1, const sol_t &sol2);
std::vector<sol_t>
reproduction(const graph &g, par_t &par,
             const std::vector<std::vector<bool>> &partitions,
             const std::vector<sol_t> &population);
std::vector<sol_t> selection(const graph &g, par_t &par,
                             const std::vector<sol_t> &population);
void remove_redundant(const graph &g, sol_t &sol);
bool is_vc(const graph &g, const sol_t sol);
sol_t best_solution(const graph &g, par_t &par,
                    const std::vector<sol_t> &population);

int main(int argc, char **argv) {
  graph g;
  std::vector<sol_t> population;
  std::vector<std::vector<bool>> partitions;
  par_t par;

  init(argc, argv, g, population, par, partitions);

  sol_t best_sol(best_solution(g, par, population));
  int best_sol_size = std::count(best_sol.begin(), best_sol.end(), true);
  while (!signalled) {
    // Calculate the new generation.
    population = reproduction(g, par, partitions, population);
    std::bernoulli_distribution dist(par.mutation_probability);
    for (sol_t &sol : population)
      if (dist(par.rng)) {
        mutation(g, par, sol);
        remove_redundant(g, sol);
      }
    population = selection(g, par, population);

    // If we have a new best solution, update best_sol.
    sol_t cur_best(best_solution(g, par, population));
    int cur_best_size = std::count(cur_best.begin(), cur_best.end(), true);
    if (cur_best_size < best_sol_size) {
      best_sol_size = cur_best_size;
      best_sol = cur_best;
    }
  }

  // Print the solution.
  std::cout << "s vc " << g.nr_vertices() << " " << best_sol_size << std::endl;
  for (size_t v = 0; v < g.nr_vertices(); v++)
    if (best_sol[v])
      std::cout << v + 1 << std::endl;

  return 0;
}

void sig_handler(int par) { signalled = true; }

// Calculate partitions of nodes, that are balanced and have a low cut.
std::vector<bool> get_partition(const graph &g, par_t &par) {
  std::uniform_int_distribution<int> dist1(0, g.nr_vertices() - 1);
  std::uniform_int_distribution<int> dist2(0, g.nr_vertices() - 2);
  int v1 = dist1(par.rng);
  int v2 = dist2(par.rng);
  if (v2 >= v1)
    v2++;

  std::vector<bool> partition(g.nr_vertices());
  std::vector<bool> reached(g.nr_vertices(), false);
  std::vector<int> next1(1, v1), next2(1, v2);
  reached[v1] = true;
  partition[v1] = true;
  reached[v2] = true;
  partition[v2] = false;
  while (!next1.empty() || !next2.empty()) {
    std::vector<int> new_next1, new_next2;
    for (int v : next1)
      for (int w : g.neighbors(v))
        if (!reached[w]) {
          reached[w] = true;
          new_next1.push_back(w);
          partition[w] = true;
        }
    for (int v : next2)
      for (int w : g.neighbors(v))
        if (!reached[w]) {
          reached[w] = true;
          new_next2.push_back(w);
          partition[w] = false;
        }
    next1 = new_next1;
    next2 = new_next2;
  }

  return partition;
}

void init(int argc, char **argv, graph &g, std::vector<sol_t> &population,
          par_t &par, std::vector<std::vector<bool>> &partitions) {
  // Set up handling of SIGTERM and SIGINT.
  signalled = false;
  signal(SIGTERM, sig_handler);
  signal(SIGINT, sig_handler);

  // Print usage message, if the parameters are not properly given.
  if (argc != 11) {
    std::cerr << "Usage: " << argv[0] << " <rand-seed> <pop-size>"
              << " <reproduction-number> <conflict-penalty> <select-out-of>"
              << " <mutation-probability> <mutation-ratio> <flip-probability>"
              << " <nr-partitions> <complement-ratio>" << std::endl;
    exit(1);
  }

  // Read graph.
  int n;
  std::list<std::pair<int, int>> e(edges_from_hgr(std::cin, n));
  g = graph(n, e);

  // Read parameters.
  par.rng = std::mt19937(std::stoi(argv[1]));
  par.population_size = std::stoi(argv[2]);
  par.reproduction_number = std::stoi(argv[3]);
  par.conflict_penalty = std::stod(argv[4]);
  par.select_out_of = std::stoi(argv[5]);
  par.mutation_probability = std::stod(argv[6]);
  par.mutation_ratio = std::stod(argv[7]);
  par.flip_probability = std::stod(argv[8]);
  par.nr_partitions = std::stoi(argv[9]);
  par.complement_ratio = std::stod(argv[10]);

  // Check, if the parameters are consistent.
  if (par.population_size <= 0 || par.reproduction_number <= 0 ||
      par.select_out_of <= 0 || par.nr_partitions <= 0) {
    std::cerr << "Population size, reproduction number, select_out_of "
              << "and the number of partitions have to be >= 1." << std::endl;
    exit(1);
  } else if (par.select_out_of > int(par.population_size)) {
    std::cerr << "select_out_of has to be less or equal to population size."
              << std::endl;
    exit(1);
  } else if (par.mutation_ratio < 0 || par.mutation_ratio > 1 ||
             par.flip_probability < 0 || par.flip_probability > 1 ||
             par.mutation_probability < 0 || par.mutation_probability > 1 ||
             par.complement_ratio < 0 || par.complement_ratio > 1) {
    std::cerr << "Mutation probability, mutation ratio, complement ratio and "
              << "flip probability have to be in the range [0,1]." << std::endl;
    exit(1);
  }

  // Initialize population according to par.population_size.
  population.clear();
  population.resize(par.population_size, sol_t(g.nr_vertices()));
  for (size_t i = 0; i < par.population_size; i++) {
    std::bernoulli_distribution dist(double(i) /
                                     double(par.population_size - 1));
    for (size_t j = 0; j < g.nr_vertices(); j++)
      population[i][j] = dist(par.rng);
  }

  partitions.clear();
  for (int i = 0; i < par.nr_partitions; i++)
    partitions.push_back(get_partition(g, par));
}

unsigned int count_unreached_edges(const graph &g, const sol_t sol) {
  assert(sol.size() == g.nr_vertices());
  int count = 0;
  for (int v : g)
    if (!sol[v])
      for (int w : g.neighbors(v))
        if (!sol[w])
          count++;
  return count / 2;
}

double fitness(const graph &g, par_t &par, const sol_t &sol) {
  return -(double)(count_unreached_edges(g, sol)) * par.conflict_penalty -
         (double)(std::count(sol.begin(), sol.end(), true));
}

/**
 * Helper function to generate a random subset
 * Picks n values at random and places them in front of the vector. The replaced
 * values are stored in the old positions of the chosen values.
 */
template <class T, class RNG>
void choose_n_randomly(std::vector<T> &vec, RNG &rng, size_t n) {
  size_t length = vec.size();
  assert(n <= length);

  for (size_t i = 0; i < n; i++) {
    std::uniform_int_distribution<> dis(i, length - 1);
    int sel = dis(rng);
    T tmp = vec[sel];
    vec[sel] = vec[i];
    vec[i] = tmp;
  }
}

void mutation(const graph &g, par_t &par, sol_t &sol) {
  std::vector<int> mutating_indices(sol.size());
  std::iota(std::begin(mutating_indices), std::end(mutating_indices), 0);

  int sample_size = ceil(par.mutation_ratio * sol.size());

  choose_n_randomly(mutating_indices, par.rng, sample_size);

  std::bernoulli_distribution dis(par.flip_probability);
  for (int i = 0; i < sample_size; i++)
    if (dis(par.rng))
      sol[mutating_indices[i]] = !sol[mutating_indices[i]];
}

sol_t cross_over(const graph &g, par_t &par,
                 const std::vector<std::vector<bool>> &partitions,
                 const sol_t &sol1, const sol_t &sol2) {
  assert(sol1.size() == g.nr_vertices());
  assert(sol2.size() == g.nr_vertices());

  sol_t combined(g.nr_vertices());
  std::uniform_int_distribution<int> dist(0, par.nr_partitions - 1);
  const std::vector<bool> &part(partitions[dist(par.rng)]);
  for (size_t i = 0; i < sol1.size(); i++) {
    if (part[i])
      combined[i] = sol1[i];
    else
      combined[i] = sol2[i];
  }
  return combined;
}

/**
 *  Returns a "solution" with all nodes selected, that are part of an
 *  uncovered edge in the given solution
 */
sol_t conflict_set(const graph &g, const sol_t &sol) {
  assert(sol.size() == g.nr_vertices());
  sol_t result(g.nr_vertices(), false);

  for (int v : g)
    if (!sol[v])
      for (int w : g.neighbors(v))
        if (!sol[w]) {
          result[v] = true;
          result[w] = true;
        }
  return result;
}

sol_t cross_over1(const graph &g, par_t &par,
                  const std::vector<std::vector<bool>> &partitions,
                  const sol_t &sol1, const sol_t &sol2) {
  assert(sol1.size() == g.nr_vertices());
  assert(sol2.size() == g.nr_vertices());

  sol_t conflict1 = conflict_set(g, sol1);
  sol_t conflict2 = conflict_set(g, sol2);

  std::vector<bool> combined(g.nr_vertices());
  std::bernoulli_distribution dist(0.5);
  for (size_t i = 0; i < sol1.size(); i++) {
    if ((conflict1[i] && sol2[i]) || (conflict2[i] && sol1[i]))
      combined[i] = true;
    else if (dist(par.rng))
      combined[i] = sol1[i];
    else
      combined[i] = sol2[i];
  }
  return combined;
}

std::vector<sol_t>
reproduction(const graph &g, par_t &par,
             const std::vector<std::vector<bool>> &partitions,
             const std::vector<sol_t> &population) {
  assert(population.size() == par.population_size);
  for (const sol_t &sol : population)
    assert(sol.size() == g.nr_vertices());

  std::uniform_int_distribution<int> dist(0, par.population_size - 1);
  std::vector<sol_t> new_generation(population);
  for (size_t i = 0; i < par.reproduction_number; i++)
    new_generation.push_back(cross_over(g, par, partitions,
                                        population[dist(par.rng)],
                                        population[dist(par.rng)]));
  assert(new_generation.size() ==
         par.population_size + par.reproduction_number);
  return new_generation;
}

std::vector<sol_t> selection(const graph &g, par_t &par,
                             const std::vector<sol_t> &population) {

  std::vector<double> fit(population.size(), 0);
  for (size_t i = 0; i < population.size(); i++)
    fit[i] = fitness(g, par, population[i]);

  std::vector<sol_t> result(par.population_size);

  std::vector<size_t> indices(population.size()); // helper vector for selecting
  std::iota(std::begin(indices), std::end(indices), 0);

  for (size_t i = 0; i < par.population_size; i++) {
    choose_n_randomly(indices, par.rng, par.select_out_of);
    int best_idx = 0;
    double best_fit = -std::numeric_limits<double>::max();
    for (int j = 0; j < par.select_out_of; j++) {
      if (fit[indices[j]] > best_fit) {
        best_idx = indices[j];
        best_fit = fit[indices[j]];
      }
    }
    result[i] = population[best_idx];
  }

  return result;
}

bool all_neighbors_in(const graph &g, int v, const sol_t &sol) {
  for (int n : g.neighbors(v))
    if (!sol[n])
      return false;
  return true;
}
void remove_redundant(const graph &g, sol_t &sol) {
  for (int v : g)
    if (all_neighbors_in(g, v, sol))
      sol[v] = false;
}

bool is_vc(const graph &g, const sol_t sol) {
  assert(sol.size() == g.nr_vertices());
  for (int v : g)
    if (!sol[v])
      for (int w : g.neighbors(v))
        if (!sol[w])
          return false;
  return true;
}

int calc_n_edges(const graph &g, int v) {
  int n_edges = 0;
  const std::unordered_set<int> &n = g.neighbors(v);
  for (auto n_it1 = n.begin(), it_end = n.end(); n_it1 != it_end; ++n_it1)
    for (auto n_it2 = n_it1; n_it2 != it_end; ++n_it2)
      if (g.adj(*n_it1, *n_it2) && *n_it1 != *n_it2)
        n_edges++;
  return n_edges;
}

double priority_function(const graph &g, int v) {
  return -(g.deg(v) - sqrt(calc_n_edges(g, v)));
}

std::unordered_set<int> vc_heur(const graph &g) {
  graph g1(g);

  // repeatedly select vertices with the lowest value of: deg - sqrt(#edges
  // between neighbors)

  std::unordered_set<int> sel;
  for (int v = 0; size_t(v) < g1.nr_vertices(); v++)
    if (g1.is_vertex(v) && g1.adj(v, v)) {
      g1.rem_vertex(v);
      sel.insert(v);
    }

  std::unordered_map<int, double> priority;
  std::priority_queue<std::pair<double, int>> next_is_v;
  for (int v : g1) {
    priority[v] = priority_function(g1, v);
    next_is_v.push(std::make_pair(priority[v], v));
  }

  while (g1.nr_vertices() > 0) {
    assert(!next_is_v.empty());
    std::pair<double, int> v(next_is_v.top());
    next_is_v.pop();
    if (v.first != priority[v.second] || !g1.is_vertex(v.second))
      continue;

    std::unordered_set<int> ns(g1.neighbors(v.second));
    std::unordered_set<int> nns(g1.neighbors(ns.begin(), ns.end()));

    g1.rem_vertex(v.second);
    for (int n : ns) {
      if (n != v.second)
        g1.rem_vertex(n);
      sel.insert(n);
    }
    for (int nn : nns)
      if (g1.is_vertex(nn)) {
        priority[nn] = priority_function(g1, nn);
        next_is_v.push(std::make_pair(priority[nn], nn));
      }
  }
  return sel;
}

sol_t complement_sol(const graph &g, const sol_t &sol) {
  assert(g.nr_vertices() == sol.size());
  std::vector<int> nsol;
  for (int v : g)
    if (!sol[v])
      nsol.push_back(v);
  graph subg(g.subgraph(nsol.begin(), nsol.end()));
  std::unordered_set<int> add(vc_heur(subg));
  sol_t c(sol);
  for (int v : add)
    c[v] = true;
  assert(is_vc(g, c));
  return c;
}

sol_t best_solution(const graph &g, par_t &par,
                    const std::vector<sol_t> &population) {
  sol_t best(g.nr_vertices(), true);
  int best_size = g.nr_vertices();

  std::vector<double> fit(population.size(), 0);
  for (size_t i = 0; i < population.size(); i++)
    fit[i] = fitness(g, par, population[i]);

  std::vector<size_t> indices(population.size());
  std::iota(std::begin(indices), std::end(indices), 0);

  std::sort(indices.begin(), indices.end(),
            [&fit](size_t i, size_t j) { return fit[i] > fit[j]; });

  for (int i = 0; i < ceil(population.size() * par.complement_ratio); i++) {
    sol_t c(complement_sol(g, population[indices[i]]));
    int size = std::count(c.begin(), c.end(), true);
    if (size < best_size) {
      best = c;
      best_size = size;
    }
  }
  return best;
}
