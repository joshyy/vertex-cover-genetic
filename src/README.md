Description
===========

A genetic algorithm for vertex cover.

Synopsis
========

./vc-genetic <rand-seed> <pop-size> <reproduction-number> <conflict-penalty>
  <select-out-of> <mutation-probability> <mutation-ratio> <flip-probability>
  <nr-partitions> <complement-ratio> < graph.gr

Prints the best found vertex cover after receiving SIGINT or SIGTERM.
Use together with *timeout* to run for a specific time.
