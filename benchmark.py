#Usage: ./vc-genetic <rand-seed> <pop-size> <reproduction-number> <conflict-penalty> <select-out-of> <mutation-probability> <mutation-ratio> <flip-probability> <nr-partitions> <complement-ratio>

from argparse import ArgumentParser;
import os;

parser = ArgumentParser()
parser.add_argument("instance", nargs="?");
parser.add_argument("algo", nargs="?");
parser.add_argument("instance_specifics", nargs="?");
parser.add_argument("runtime_cutoff", nargs="?");
parser.add_argument("runlength", nargs="?");
parser.add_argument("-N", "--pop-size", dest="pop_size");
parser.add_argument("-r", "--reproduction-number", dest="reproduction_number");
parser.add_argument("-c", "--conflict-penalty", dest="conflict_penalty");
parser.add_argument("-o", "--select-out-of", dest="select_out_of");
parser.add_argument("-p", "--mutation-probability", dest="mutation_probability");
parser.add_argument("-m", "--mutation-ratio", dest="mutation_ratio");
parser.add_argument("-f", "--flip-probability", dest="flip_probability");
parser.add_argument("-n", "--nr-partitions", dest="nr_partitions");
parser.add_argument("-a", "--complement-ratio", dest="complement_ratio");
parser.add_argument("-s", "--random-seed", dest="random_seed");


args = parser.parse_args();

all_instances = ['src\\inst\\{0:03}.gr'.format(i) for i in range(57, 75, 2)]

print("instance;result")
for inst in all_instances:
    output = os.popen('"C:\\Program Files\\Git\\usr\\bin\\timeout.exe" 60s "Release\\Vertex Cover Genetic.exe" ' + str(args.random_seed) + " " + args.pop_size + " " + args.reproduction_number +
            " " + args.conflict_penalty + " " + args.select_out_of + " " + args.mutation_probability +
            " " + args.mutation_ratio + " " + args.flip_probability +
            " " + args.nr_partitions + " " + "0.7" + " < " + inst).read()
    try:    
        val = int(output.split('\n')[-2].split(' ')[0])
    except:
        try:
            val = int(output.split(' ')[0])
        except:
            val = ''
    print('{0};{1}'.format(inst, val))
