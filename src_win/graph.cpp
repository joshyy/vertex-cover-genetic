#include "graph.h"

#include "unionfind.h"

#include <algorithm>
#include <limits>
#include <list>
#include <sstream>

// ############################################################################
// Reading the adjacency matrix from an instream                            ###
// ############################################################################

std::string next_line(std::istream &stm) {
  std::string line{""};
  do
    getline(stm, line);
  while ((line[0] == 'c' || line[0] == 'C' || line == "") && stm);
  return line;
}

std::list<std::pair<int, int>> edges_from_hgr(std::istream &stm, int &n) {
  int m;
  std::string line, tmp;

  line = next_line(stm);
  assert(stm);

  std::stringstream ss(line);
  ss >> tmp >> tmp;
  ss >> n;
  ss >> m;

  std::list<std::pair<int, int>> edges;

  while (stm.peek() != EOF) {
    int v1, v2;
    line = next_line(stm);
    std::stringstream ss1(line);
    ss1 >> v1 >> v2;
    edges.push_back(std::make_pair(v1 - 1, v2 - 1));
  }

  return edges;
}

// ############################################################################
// Checking graph consistency                                               ###
// ############################################################################

bool graph::is_consistent() const {
  // 'vertices' is the set of vertices.
  for (int v : vertices)
    if (adj_sets.count(v) == 0)
      return false;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    if (vertices.count(p.first) == 0)
      return false;

  // All neighbors are vertices.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (!is_vertex(v))
        return false;

  // Each edge is undirected.
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    for (int v : p.second)
      if (adj_sets.at(v).count(p.first) == 0)
        return false;

  // 'm' is the number of edges.
  int _m = 0;
  for (const std::pair<int, std::unordered_set<int>> &p : adj_sets)
    _m += p.second.size() + p.second.count(p.first);
  if (_m != 2 * m)
    return false;

  // 'next_free_vertex' is larger than the identifiers of all vertices
  for (int v : vertices)
    if (next_free_vertex <= v)
      return false;

  return true;
}

// ############################################################################
// Constructors                                                             ###
// ############################################################################

graph::graph() : m(0), next_free_vertex(0), vertices(), adj_sets() {}

graph::graph(const graph &g)
    : m(g.m), next_free_vertex(g.next_free_vertex), vertices(g.vertices),
      adj_sets(g.adj_sets) {}

graph::graph(int n) : m(0), next_free_vertex(n), vertices(n), adj_sets(n) {
  for (int i = 0; i < n; i++)
    add_vertex(i);
  assert(is_consistent());
}

graph::graph(int n, const std::list<std::pair<int, int>> &edges) : graph(n) {
  for (std::pair<int, int> e : edges)
    if (!adj(e.first, e.second)) {
      adj_sets[e.first].insert(e.second);
      if (e.first != e.second)
        adj_sets[e.second].insert(e.first);
      m++;
    }
  assert(is_consistent());
}

// ############################################################################
// Graph Manipulation                                                       ###
// ############################################################################

void graph::add_edge(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 0);
  assert(adj_sets[w].count(v) == 0);
  adj_sets[v].insert(w);
  adj_sets[w].insert(v);
  m++;
}

void graph::add_vertex(int v) {
  assert(vertices.count(v) == 0);
  assert(adj_sets.count(v) == 0);
  vertices.insert(v);
  adj_sets[v];
  assert(v < std::numeric_limits<int>::max());
  if (next_free_vertex <= v)
    next_free_vertex = v + 1;
}

void graph::rem_edge(int v, int w) {
  assert(adj_sets.count(v) == 1);
  assert(adj_sets.count(w) == 1);
  assert(adj_sets[v].count(w) == 1);
  assert(adj_sets[w].count(v) == 1);
  adj_sets[v].erase(w);
  adj_sets[w].erase(v);
  m--;
}

void graph::rem_vertex(int v) {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  m -= deg(v);
  for (int n : neighbors(v))
    if (n != v)
      adj_sets[n].erase(v);
  vertices.erase(v);
  adj_sets.erase(v);
}

// ############################################################################
// Obtaining Information                                                    ###
// ############################################################################

size_t graph::nr_vertices() const { return vertices.size(); }

size_t graph::nr_edges() const { return m; }

bool graph::is_vertex(int v) const { return vertices.count(v) == 1; }

int graph::free_vertex() const { return next_free_vertex; }

size_t graph::deg(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v).size();
}

bool graph::adj(int v, int w) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  assert(vertices.count(w) == 1);
  assert(adj_sets.count(w) == 1);
  return adj_sets.at(v).count(w) == 1;
}

std::unordered_set<int>::const_iterator graph::begin() const {
  return vertices.begin();
}
std::unordered_set<int>::const_iterator graph::end() const {
  return vertices.end();
}

const std::unordered_set<int> &graph::neighbors(int v) const {
  assert(vertices.count(v) == 1);
  assert(adj_sets.count(v) == 1);
  return adj_sets.at(v);
}

std::vector<std::vector<int>> graph::conn_comp() const {
  std::vector<std::vector<int>> cc;
  unionfind u(begin(), end());
  for (int v : *this)
    for (int n : neighbors(v))
      u.unify(v, n);
  std::unordered_map<int, int> comp;
  for (int v : *this) {
    int root = u.find(v);
    if (comp.count(root) == 0) {
      cc.resize(cc.size() + 1);
      comp[root] = cc.size() - 1;
    }
    cc.at(comp[root]).push_back(v);
  }
  return cc;
}

// ############################################################################
// Isomorphism                                                              ###
// ############################################################################

bool graph::is_isomorphism(const graph &g,
                           const std::unordered_map<int, int> &iso) const {
  if (this->nr_vertices() != g.nr_vertices() ||
      this->nr_edges() != g.nr_edges())
    return false;
  for (int v : *this)
    for (int w : *this)
      if (adj(v, w) != g.adj(iso.at(v), iso.at(w)))
        return false;
  return true;
}

std::unordered_map<int, int> graph::characteristic(int n) const {
  std::unordered_map<int, int> ch, tmp;
  for (int v : *this)
    ch[v] = 1;
  for (int i = 0; i < n; i++) {
    tmp = ch;
    for (int v : *this) {
      ch[v] = 0;
      for (int w : neighbors(v))
        ch[v] += tmp[w];
    }
  }
  return ch;
}

std::unordered_map<int, int> values(std::unordered_map<int, int> m) {
  std::unordered_map<int, int> vals;
  for (const std::pair<int, int> &p : m)
    vals[p.second]++;
  return vals;
}

std::unordered_map<int, int>
graph::isomorphic_to_con(const graph &g, std::unordered_map<int, int> iso,
                         std::unordered_map<int, int> iso_inv,
                         const std::unordered_map<int, int> &ch,
                         const std::unordered_map<int, int> &chg,
                         std::list<int> order) const {
  assert(iso.size() + order.size() == nr_vertices());
  assert(iso.size() == iso_inv.size());
  if (order.empty())
    return iso;
  int v = order.front();
  order.pop_front();
  for (int vg : g)
    if (ch.at(v) == chg.at(vg) && iso_inv.count(vg) == 0) {
      bool addable = true;
      for (int n : neighbors(v))
        if (iso.count(n) == 1)
          addable = addable && g.adj(vg, iso[n]);
      for (int ng : g.neighbors(vg))
        if (iso_inv.count(ng) == 1)
          addable = addable && adj(v, iso_inv[ng]);
      if (addable) {
        iso[v] = vg;
        iso_inv[vg] = v;
        std::unordered_map<int, int> res =
            isomorphic_to_con(g, iso, iso_inv, ch, chg, order);
        if (!res.empty())
          return res;
        iso.erase(v);
        iso_inv.erase(vg);
      }
    }
  return std::unordered_map<int, int>();
}

bool graph::isomorphic_to_con(
    const graph &g, std::unordered_map<int, int> &isomorphism,
    const std::unordered_map<int, int> &subiso) const {
  assert(conn_comp().size() == 1);
  assert(g.conn_comp().size() == 1);
  assert(static_cast<int>(g.conn_comp().size()) == 1);
  std::vector<std::vector<int>> cc(g.conn_comp());
  isomorphism = std::unordered_map<int, int>();
  if (nr_vertices() != g.nr_vertices() || nr_edges() != g.nr_edges())
    return false;
  std::unordered_map<int, int> ch = characteristic(2);
  std::unordered_map<int, int> chg = g.characteristic(2);
  std::unordered_map<int, int> vals(values(ch));
  if (vals != values(chg))
    return false;
  std::list<int> order;
  for (int v : *this)
    if (subiso.count(v) == 0)
      order.push_back(v);
  order.sort([&vals](int x, int y) { return vals[x] > vals[y]; });
  std::unordered_map<int, int> subiso_inv;
  for (const std::pair<int, int> &p : subiso)
    subiso_inv[p.second] = p.first;
  isomorphism = isomorphic_to_con(g, subiso, subiso_inv, ch, chg, order);
  if (!isomorphism.empty())
    assert(is_isomorphism(g, isomorphism));
  return !isomorphism.empty();
}

bool graph::operator==(const graph &g) const { return isomorphic_to(g); }

bool graph::isomorphic_to(const graph &g) const {
  std::unordered_map<int, int> tmp;
  return isomorphic_to(g, tmp);
}

bool graph::isomorphic_to(const graph &g,
                          std::unordered_map<int, int> &isomorphism) const {
  return isomorphic_to(g, isomorphism, std::unordered_map<int, int>());
}

bool graph::isomorphic_to(const graph &g,
                          std::unordered_map<int, int> &isomorphism,
                          const std::unordered_map<int, int> &subiso) const {
  isomorphism = std::unordered_map<int, int>();
  std::vector<std::vector<int>> _cc(conn_comp());
  std::vector<std::vector<int>> _ccg(g.conn_comp());
  if (_cc.size() != _ccg.size())
    return false;
  std::list<graph> cc, ccg;
  for (const std::vector<int> &c : _cc)
    cc.push_back(subgraph(c.begin(), c.end()));
  for (const std::vector<int> &c : _ccg)
    ccg.push_back(g.subgraph(c.begin(), c.end()));
  std::list<std::unordered_map<int, int>> subsubiso;
  for (const graph &c : cc) {
    subsubiso.push_back(std::unordered_map<int, int>());
    for (int v : c)
      if (subiso.count(v) == 1)
        subsubiso.back()[v] = subiso.at(v);
  }

  std::unordered_map<int, int> iso;
  while (!cc.empty()) {
    std::unordered_map<int, int> sub_iso;
    std::list<graph>::iterator it(ccg.begin());
    std::list<std::unordered_map<int, int>>::iterator it1(subsubiso.begin());
    while (it != ccg.end() && !cc.front().isomorphic_to_con(*it, sub_iso, *it1))
      ++it, ++it1;
    if (it == ccg.end())
      return false;
    iso.insert(sub_iso.begin(), sub_iso.end());
    cc.pop_front();
    subsubiso.pop_front();
    ccg.erase(it);
  }
  isomorphism = iso;
  return true;
}
