#ifndef GRAPH_H_
#define GRAPH_H_

#include <cassert>
#include <istream>
#include <list>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// Reads all edges from a stream, where the data is in the hgr- or gr-format
// (https://pacechallenge.org/2019/vc/vc_format/) and returns a list of pairs.
// Each pair represents an edge.
std::list<std::pair<int, int>> edges_from_hgr(std::istream &stm, int &n);

class graph {
private:
  // Number of edges
  int m;

  // A number, that is larger than the identifiers of all vertices in the
  // graph
  int next_free_vertex;

  // Set of the vertices in the graph
  std::unordered_set<int> vertices;

  // Sets of neighbors for each vertex
  std::unordered_map<int, std::unordered_set<int>> adj_sets;

  // Returns true, if all of the stored data is consistent and false otherwise.
  bool is_consistent() const;

  // Returns true, if 'iso' is an isomorphism from this graph to 'g'.
  bool is_isomorphism(const graph &g,
                      const std::unordered_map<int, int> &iso) const;

  // Calculates a number for each vertex in a deterministic way (i.e. for an
  // isomorphic graph two corresponding vertices have the same number).
  std::unordered_map<int, int> characteristic(int n) const;

  // A helper function for isomorphic_to_con(const graph &,
  // std::unordered_map<int, int> &)
  std::unordered_map<int, int>
  isomorphic_to_con(const graph &g, std::unordered_map<int, int> iso,
                    std::unordered_map<int, int> iso_inv,
                    const std::unordered_map<int, int> &ch,
                    const std::unordered_map<int, int> &chg,
                    std::list<int> order) const;

  // Same as isomorpic_to(const graph &, std::unordered_map<int, int> &) except
  // the fact, that this function only works, if both graphs (*this, g) are
  // connected.
  bool isomorphic_to_con(const graph &g,
                         std::unordered_map<int, int> &isomorphism,
                         const std::unordered_map<int, int> &subiso) const;

public:
  // ##########################################################################
  // Constructors                                                           ###
  // ##########################################################################

  // Initializes the graph without any vertices and edges.
  graph();

  // Initializes the graph from another graph.
  graph(const graph &g);

  // Initializes the graph with n vertices with no edges in between. The
  // identifiers of the vertices are given by {0, 1, ..., n-1}.
  graph(int n);

  // Initializes the graph with n vertices (as for graph(int n)) and adds edges
  // as given by 'edges'. Double edges are ignored.
  graph(int n, const std::list<std::pair<int, int>> &edges);

  // ##########################################################################
  // Graph Manipulation                                                     ###
  // ##########################################################################

  // Adds an edge between the nodes 'v' and 'w'. Result is undefined, if one of
  // the vertices does not exist or the edge is already present.
  void add_edge(int v, int w);

  // Adds the vertex 'v' to the graph. Result is undefined, if the vertex
  // already exists.
  void add_vertex(int v);

  // Removes the edge between 'v' and 'w'. Result is undefined, if one of the
  // vertices does not exist or the edge is not present.
  void rem_edge(int v, int w);

  // Removes the vertex 'v' and all edges 'v' is adjacent to. Result is
  // undefined, if the vertex does not exist.
  void rem_vertex(int v);

  // ##########################################################################
  // Obtaining Information                                                  ###
  // ##########################################################################

  // Returns the number of vertices in the graph.
  size_t nr_vertices() const;

  // Returns the number of edges in the graph.
  size_t nr_edges() const;

  // Returns true, if 'v' is a vertex of the graph and false otherwise.
  bool is_vertex(int v) const;

  // Returns an integer, that can be added as vertex (i.e.
  // is_vertex(free_vertex()) always evaluates to false).
  int free_vertex() const;

  // Returns the degree of the vertex 'v'. Result is undefined, if the vertex
  // does not exist.
  size_t deg(int v) const;

  // Returns true, if 'v' and 'w' are adjacent (i.e. there is an edge in
  // between them), and false otherwise. Result is undefined, if one of the
  // vertices does not exist.
  bool adj(int v, int w) const;

  // Return iterators to iterate through all vertices of the graph. Iterators
  // are valid until a change to the graph is made.
  std::unordered_set<int>::const_iterator begin() const;
  std::unordered_set<int>::const_iterator end() const;

  // Returns the set of neighbors for the vertex 'v' (or the vertices given by
  // 'begin' and 'end'). For the first version the set is valid until the graph
  // is changed. 'closed_neighbors' also includes the given vertices.
  const std::unordered_set<int> &neighbors(int v) const;
  template <typename ForwardIterator>
  std::unordered_set<int> neighbors(ForwardIterator begin,
                                    ForwardIterator end) const;
  template <typename ForwardIterator>
  std::unordered_set<int> closed_neighbors(ForwardIterator begin,
                                           ForwardIterator end) const;

  // Returns a vector of all connected components of the graph.
  std::vector<std::vector<int>> conn_comp() const;

  // Get a subgraph of the graph.
  template <typename ForwardIterator>
  graph subgraph(ForwardIterator begin, ForwardIterator end) const;

  // Returns true, if this graph is isomorphic to 'g' and false otherwise.
  bool operator==(const graph &g) const;
  bool isomorphic_to(const graph &g) const;

  // Returns true, if this graph is isomorphic to 'g'. If so, sets
  // 'isomorphism' to an isomorphism from this graph to 'g'. Otherwise returns
  // false and sets 'isomorphism' to the empty map.
  bool isomorphic_to(const graph &g,
                     std::unordered_map<int, int> &isomorphism) const;

  // Returns true, if there is an isomorphism to 'g', that extends 'subiso'. If
  // so, sets 'isomorphism' to such an isomorphism. Otherwise returns false and
  // sets 'isomorphism' to the empty map.
  bool isomorphic_to(const graph &g, std::unordered_map<int, int> &isomorphism,
                     const std::unordered_map<int, int> &subiso) const;
};

template <typename ForwardIterator>
std::unordered_set<int> graph::neighbors(ForwardIterator begin,
                                         ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  std::unordered_set<int> ns;
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(neighbors(*it).begin(), neighbors(*it).end());
  for (ForwardIterator it = begin; it != end; ++it)
    ns.erase(*it);

  return ns;
}
template <typename ForwardIterator>
std::unordered_set<int> graph::closed_neighbors(ForwardIterator begin,
                                                ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  std::unordered_set<int> ns;
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(neighbors(*it).begin(), neighbors(*it).end());
  for (ForwardIterator it = begin; it != end; ++it)
    ns.insert(*it);

  return ns;
}

template <typename ForwardIterator>
graph graph::subgraph(ForwardIterator begin, ForwardIterator end) const {
  for (ForwardIterator it = begin; it != end; ++it)
    assert(is_vertex(*it));

  graph s;
  for (ForwardIterator it = begin; it != end; ++it)
    s.add_vertex(*it);
  for (ForwardIterator it = begin; it != end; ++it)
    for (int n : neighbors(*it))
      if (*it <= n && s.is_vertex(n))
        s.add_edge(*it, n);
  assert(s.is_consistent());
  return s;
}

#endif // GRAPH_H_
